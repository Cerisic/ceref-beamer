# CeREFBeamer

This repository is a Beamer theme package designed to help CeREF researchers writing their presentation with Beamer. An usage example is given in the file `example_CeREF.tex`.


## Loading the theme
### Introduction
The simplest way to use this Beamer theme package, is to use it as a local package (that's why we kept all the features inside one file). To do so, download the repository and edit the `example_CeREF.tex` file.

This theme package allows you to load Beamer as a class (as usual) and pass any options you are used to directly to the beamer class. Nevertheless, we recommend using the `aspectratio=169` option.

```
\documentclass[aspectratio=169]{beamer}
```

This means that you can also use the option `handout` to print handouts of your presentation, as follows:

```
\documentclass[aspectratio=169, handout]{beamer}
```
In order to use the theme, you will have load the Beamer theme before the \begin{document} command, as below:

```
\usetheme{ceref}
```

With all the default parameters, you presentation will have the look of a main CeREF presentation.

### Make a title slide
 In order to make a title slide, you will have to define a title, a subtitle, an author and a date, with these respective commands:

* `\title{}`
* `\subtitle{}`
* `\author{}`
* `\date{}`

All these commands should be defined before the `\begin{document}` command.

Then you can add all the different partners to the project:
* promoter
* industrialpartner
* scientificpartner
* fundingpartner

```
Example:
\usepackage[industrialpartner, scientificpartner]{CeREFBeamer}
```

Each one of these options will allow you to use the following dedicated command:
* \promoter[]{}
* \industrialpartner[]{}
* \scientificpartner[]{}
* \fundingpartner[]{}

Where you can specify the path to a logo, with the `\includegraphics{}` command, as follows:

```
Example:
\promoter[Promoteur]{\includegraphics[height=0.06\textwidth]{img/HELHa.png}}
```

## Theme options

### Option - `department`
The first option that you will have to select is the department option. This will change the main color of the presentation according to the CeREF graphical charter but also the logo that appears on the frames:

* **ceref**
* agronomique
* arts
* economique
* education
* sante
* social
* technique

This options should be used between brackets as follows:
```
\usetheme[department=education]{ceref}
```

### Options - `promoter`
By default, in the lower right corner of the slide the HELHa logo will be displayed as it is the default promoter value. If for any reason, you need to change this, you will need to specify the value for these 3 package options:

* `promoterimgpath`
* `promoternegimgpath`
* `promoterimgsize`

These options will let you change the image that will be displayed in the lower right corner of the slide (`promoterimgpath`), its size (`promoterimgsize`) and the image displayed on negative slides (`promoternegimgpath`).

Here is an example of how you can use these options (images can be saved in a different folder than the default `/assets/` folder):
* `promoterimgpath=assets/technique-phrase.pdf`
* `promoternegimgpath=assets/technique-white.pdf`
* `promoterimgsize=0.1`


### Option - `innertheme`

The `innertheme` option allows you to specify the inner theme used in your presentation. The accepted values for this option are:
* **default**
* circles
* rectangles
* rounded
* inmargin

This will mainly affect the look of the itemized list and the blocks. If not provided this options will default to `default`.


### Option - `outertheme`
The `outertheme` option allows you to specify the outer theme used in your presentation. The accepted values for this option are:
* **default**
* infolines
* miniframes
* shadow
* sidebar
* smoothbars
* smoothtree
* split
* tree

This will mainly affect how your content is rendered on the slide. If not provided this options will default to `default`.

**Note:** For the moment only the `default` outer theme is supported with the background presentation of the template.

### Option - `itemize`
If you want to change the layout only of the itemize items. 
* default
* triangle
* circle
* ball
* square
* enumeration

### Option - `negativeblocks`

If the `negativeblocks` option is used, the normal blocks will be displayed with the main color of the presentation as the title background color. The title text will be displayed in white. The content of the block will be in a slightly gray box.

In opposition (if the option is not used) the normal blocks will used a dark gray background color for the title of the block. The title text will be display with the main color of the presentation.


## Frame options


### Regular frame

### Frame flavors

* plain
* titleframe
* graphicfill
* negativegraphicfill

### Vertical alignement
You can easily specify the vertical alignment of your frame contents, using the options:
* [t] for vertical
* [c] for center
* [b] for bottom

### Intermission slides

`\partframe`
`\sectionframe`
`\subsectionframe`
`\sectionsubsectionframe`

### Hooks

```
\AtBeginPart{
}
```

```
\AtBeginSection{
}
```

```
\AtBeginSubSection{
}
```
#### Examples
If you want to create a blank slide (without frame number) for each part that you create with the `\part{}` command. You can place the hook definition, before the `\begin{document}`.
```
\AtBeginPart{
	\begin{frame}[plain]
		\partframe
	\end{frame}
}
```

If you want to create a slide (without frame number but with the CeREF arrow as a background) for each section that you create with the `\part{}` command. You can place the hook definition, before the `\begin{document}`.
```
\AtBeginSection{
\begin{frame}[titleframe]
	\sectionframe
\end{frame}
}
```

### Slides with graphic material

## Specific commands


### Highlighting text
The `\hl{}` command stands for **highlight**. This command allows you to highlight words in text or paragraph. This command also changes the color of the text to match the main color of the presentation, according to the selected `department` option.


### Colors
### Logos
* `\logoceref` - icon of the defined CeREF department (defined by the `department` option)
* `\logohelha` - icon of HELHa
* `\logopromoter` - icon of the promoter (defined by the `promoter` option)
* `\logocerefceref` - icon of the CeREF 
* `\logocerefagronomique` - icon of the CeREF-Agronomique cell.
* `\logocerefarts` - icon of the CeREF-ArtsAppliqué cell.
* `\logocerefeconomique` - icon of the CeREF-Économique cell.
* `\logocerefeducation` - icon of the CeREF-Éducation cell.
* `\logocerefsante` - icon of the CeREF-Santé.
* `\logocerefsocial` - icon of the CeRSO cell.
* `\logocereftechnique` - icon of the CeREF-Technique cell

For each of these icons you specify the scale by using the bracketed option. For example:

```
\logoceref[0.3]
```

Will apply a scale of 0.3 to the current CeREF logo (the default value for the scale is 0.2). 

In addition the package provides a `\place` command which will allow you to specify the location of the logo.

#### Example
In the following example, we place the HELHa icon in the center of the slide (the $x$ and $y$ coordintates are normalized).

```
\place at (0.5,0.5) {\logohelha[0.2]}
```

### Shading photographs

Command `\glare[]{}{}`

#### Example
```
\begin{frame}[negativegraphicfill={\glare[0.5]{ceref-education}{\includegraphics{assets/img/computer.png}}}]

\end{frame}
```
### Block material




### After \begin{document}
This class will also give you acces to the `\sectionning{}{}` command. This command has 2 parameters the first one defines the sectionning level (i.e: Chapter, Section, ...) and the second one defines the title of this sectionning level (i.e: the title of the chapter,...):

* \sectionning

```
Example:
\sectionning[Chapitre]{Un titre de section}
```

If you want to number the sectionning levels, you can either do it manually (i.e: `Chapter 1`, ...), or use the command named `\numberedsectionning`, which can be used has follows:
```
Example:
\numberedsectionning[Chapitre]{Un titre de section}
```




## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
